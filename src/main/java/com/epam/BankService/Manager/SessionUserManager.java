package com.epam.BankService.Manager;


import com.epam.BankService.DTO.User;

public interface SessionUserManager {

    public void setCurrentSessionUser(User user);

    public User getCurrentSessionUser();
}