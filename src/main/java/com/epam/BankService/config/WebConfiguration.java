package com.epam.BankService.config;

import com.epam.BankService.interceptor.AdminInterceptor;
import com.epam.BankService.interceptor.AuthenticationInterceptor;
import com.epam.BankService.interceptor.UserAwareInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration

public class WebConfiguration extends WebMvcConfigurerAdapter {



    /**

     * Бин общего интерсептера проверки наличия аутентификации

     */

    @Bean

    public AuthenticationInterceptor authenticationInterceptor() {

        return new AuthenticationInterceptor();

    }



    /**

     * Бин интерсептера проверки наличия роли ADMIN у пользователя

     */

 @Bean

    public AdminInterceptor adminInterceptor() {

        return new AdminInterceptor();

    }



    /**

     * Бин интерсептера кладущего в модель текущего пользователя

     */

  @Bean

    public UserAwareInterceptor userAwareInterceptor() {

        return new UserAwareInterceptor();

    }



    @Override

    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(authenticationInterceptor()).addPathPatterns("/**").excludePathPatterns("/login", "/registration","home",

                "/logout", "/css/**", "/js/**", "/checkloginexist");



        registry.addInterceptor(userAwareInterceptor()).addPathPatterns("/**");



        registry.addInterceptor(adminInterceptor()).addPathPatterns("/admin");

    }




}