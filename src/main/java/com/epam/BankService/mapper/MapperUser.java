package com.epam.BankService.mapper;


import com.epam.BankService.DTO.Role;
import com.epam.BankService.DTO.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MapperUser  implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("idusers"));
        user.setLogin(resultSet.getString("login"));
        user.setUsername(resultSet.getString("username"));
        user.setPassword(resultSet.getString("password"));
        Role role;
        role = Role.valueOf(resultSet.getString("userrole"));
        user.setRole(role);
        return user;
    }
}
