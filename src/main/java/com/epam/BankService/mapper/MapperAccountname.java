package com.epam.BankService.mapper;

import com.epam.BankService.DTO.Accountname;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MapperAccountname implements RowMapper<Accountname> {
    @Override
    public Accountname mapRow(ResultSet resultSet, int i) throws SQLException {
        Accountname accountname = new Accountname();
        accountname.setId(resultSet.getInt("idaccountname"));
        accountname.setNumberaccount(resultSet.getInt("nameaccount"));
        accountname.setAccountstatus(resultSet.getBoolean("accountstatus"));
        return accountname;
    }
}
