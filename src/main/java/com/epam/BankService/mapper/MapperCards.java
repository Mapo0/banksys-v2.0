package com.epam.BankService.mapper;

import com.epam.BankService.DTO.Cards;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MapperCards implements RowMapper<Cards> {

    @Override
    public Cards mapRow(ResultSet resultSet, int i) throws SQLException {
        Cards cards = new Cards();
        cards.setId(resultSet.getInt("idcards"));
        cards.setNumbercard(resultSet.getInt("numbercard"));
        cards.setBalance(resultSet.getInt("balance"));
        return cards;
    }
}
