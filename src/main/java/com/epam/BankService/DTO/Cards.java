package com.epam.BankService.DTO;

public class Cards {
    private int id;
    private int numbercard;
    private int balance;
    private int idaccountname;

    public Cards() {
    }

    public Cards(int numbercard,int balance, int idaccountname) { this.numbercard = numbercard;
    this.balance = balance;
    this.idaccountname = idaccountname;}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumbercard() {
        return numbercard;
    }

    public void setNumbercard(int numbercard) {
        this.numbercard = numbercard;
    }

    public int getBalance() { return balance; }

    public void setBalance(int balance) { this.balance = balance; }

    public int getIdaccountname() {
        return idaccountname;
    }

    public void setIdaccountname(int idaccountname) {
        this.idaccountname = idaccountname;
    }
    /* @Override
    public String toString() {
        return "Cards{" +
                "id=" + id +
                ", numbercard=" + numbercard +
                '}';
    }*/
}
