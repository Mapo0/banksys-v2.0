package com.epam.BankService.DTO;

public enum Role {
    ADMIN,
    CLIENT;
}
