package com.epam.BankService.DTO;

public class Transactions {
    private int id;
    private double transfer;
    private double accountbalance;

    public Transactions() {
    }

    public Transactions(double transfer, double accountbalance) {
        this.transfer = transfer;
        this.accountbalance = accountbalance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTransfer() {
        return transfer;
    }

    public void setTransfer(double transfer) {
        this.transfer = transfer;
    }

    public double getAccountbalance() {
        return accountbalance;
    }

    public void setAccountbalance(double accountbalance) {
        this.accountbalance = accountbalance;
    }

    @Override
    public String toString() {
        return "Transactions{" +
                "id=" + id +
                ", transfer=" + transfer +
                ", accountbalance=" + accountbalance +
                '}';
    }
}
