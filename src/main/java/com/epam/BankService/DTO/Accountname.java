package com.epam.BankService.DTO;

public class Accountname {
    private int id;
    private int numberaccount;
    private boolean accountstatus;
    private int idusers;

   /*public Accountname(int numberaccount, boolean accountstatus) {
        this.numberaccount = numberaccount;
        this.accountstatus = accountstatus;
    }
*/
    public Accountname() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumberaccount() {
        return numberaccount;
    }

    public void setNumberaccount(int numberaccount) {
        this.numberaccount = numberaccount;
    }

    public boolean isAccountstatus() {
        return accountstatus;
    }

    public void setAccountstatus(boolean accountstatus) {
        this.accountstatus = accountstatus;
    }

    //правки
   public int getIdusers() { return idusers; }

    public void setIdusers(int idusers) { this.idusers = idusers; }


/* @Override
    public String toString() {
        return "Accountname{" +
                "id=" + id +
                ", numberaccount=" + numberaccount +
                ", accountstatus=" + accountstatus +
                '}';
    }*/
}
