package com.epam.BankService.exception;

public class CardsAlreadyExistException extends Exception {
    private static final long serialVersionUID = 9156950209954533944L;

    public CardsAlreadyExistException(String message) {
        super(message);
    }
}
