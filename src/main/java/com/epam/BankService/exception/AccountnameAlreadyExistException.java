package com.epam.BankService.exception;

public class AccountnameAlreadyExistException extends Exception {
    private static final long serialVersionUID = 9156950209954533944L;

    public AccountnameAlreadyExistException(String message) {
        super(message);
    }
}
