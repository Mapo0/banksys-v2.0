package com.epam.BankService.Repository;

import com.epam.BankService.DTO.User;

import java.util.List;

public interface UserRepository {

    List<User> getAllUsers();

    User getUserByLogin(String login);

    void addUser(User user);

    void updateUser(User user);

    void removeUserById(long id);

    void removeUserByLogin(String login);

}
