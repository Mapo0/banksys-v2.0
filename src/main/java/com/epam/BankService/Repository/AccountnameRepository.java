package com.epam.BankService.Repository;

import com.epam.BankService.DTO.Accountname;
import com.epam.BankService.DTO.User;

import java.util.List;

public interface AccountnameRepository {

    public List<Accountname> getAllAccountnameById(int id) ;


      public  List<Accountname> getAllAccountname();

    Accountname getAccountnameById(int id);

    void addAccountname(Accountname accountname , int users_idusers);

    void removeAccountnameById(long id);

    void removeAccountnameByLogin(String login);

    public void updateAccountname(Accountname accountname, int users_idusers );


}
