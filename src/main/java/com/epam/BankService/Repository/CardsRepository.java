package com.epam.BankService.Repository;

import com.epam.BankService.DTO.Cards;

import java.util.List;

public interface CardsRepository {
    List<Cards> getAllCards();

    List<Cards> getAllCardsById(int id);

    Cards getCardsById(int id);

    void addCards(Cards cards , int idaccountname);

    void removeCardsById(long id);

    void removeCardsByLogin(String login);

    public void updateCards(Cards cards, int accountname_idaccountname);

    public void updateCard(Cards cards, int accountname_idaccountname);

}
