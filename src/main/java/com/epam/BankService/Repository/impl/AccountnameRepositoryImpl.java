package com.epam.BankService.Repository.impl;

import com.epam.BankService.DTO.Accountname;
import com.epam.BankService.Repository.AccountnameRepository;
import com.epam.BankService.mapper.MapperAccountname;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class AccountnameRepositoryImpl implements AccountnameRepository {

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Accountname> getAllAccountname( ) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("Select * From accountname", new MapperAccountname());
    }
    @Override
    public List<Accountname> getAllAccountnameById(int id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("Select * From accountname where users_idusers= ?" ,new Object[]{id},  new MapperAccountname());
    }

    @Override
    public Accountname getAccountnameById(int id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("Select * From accountname where idaccountname= ?" ,new Object[]{id},  new MapperAccountname());

    }

    @Override
    public void addAccountname(Accountname accountname, int users_idusers) {

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("nameaccount", accountname.getNumberaccount())
                .addValue("accountstatus", accountname.isAccountstatus())
                .addValue("users_idusers",users_idusers);
        namedParameterJdbcTemplate.update("Insert into banksys.accountname (nameaccount,accountstatus,users_idusers) Values (:nameaccount,:accountstatus,:users_idusers)", sqlParameterSource);
    }

    @Override
    public void removeAccountnameById(long id) {

    }

    @Override
    public void removeAccountnameByLogin(String login) { }

    @Override
    public void updateAccountname(Accountname accountname, int users_idusers) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Update accountname Set nameaccount = ?, accountstatus = ?  Where idaccountname = ?",

                new Object[]{accountname.getNumberaccount(), accountname.isAccountstatus(),accountname.getId()});

    }

}
