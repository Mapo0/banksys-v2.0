package com.epam.BankService.Repository.impl;

import com.epam.BankService.DTO.User;
import com.epam.BankService.Repository.UserRepository;
import com.epam.BankService.mapper.MapperUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private DataSource dataSource;

    @Override
    public List<User> getAllUsers() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("Select * From users", new MapperUser());
    }

    @Override
    public User getUserByLogin(String login) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("Select * From users Where login = ?", new Object[]{login}, new MapperUser());
    }

    @Override
    public void addUser(User user) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("login", user.getLogin())
                .addValue("role", user.getRole().toString().toUpperCase())
                .addValue("password", user.getPassword())
                .addValue("name", user.getUsername());
        namedParameterJdbcTemplate.update("Insert into banksys.users (login,userrole,password,username) Values (:login,:role,:password,:name)", sqlParameterSource);
    }

    @Override
    public void updateUser(User user) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Update users Set   userrole = ?, password = ? , login = ? , Where username = ?" ,
                new Object[]{ user.getRole().toString().toUpperCase(), user.getPassword(),user.getLogin(), user.getUsername()});
    }

    @Override
    public void removeUserById(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Delete From users Where idusers = ?", id);

    }

    @Override
    public void removeUserByLogin(String login) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Delete From users Where login = ?", login);
    }

}
