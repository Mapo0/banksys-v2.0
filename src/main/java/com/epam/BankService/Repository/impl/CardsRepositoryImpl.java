package com.epam.BankService.Repository.impl;

import com.epam.BankService.DTO.Cards;
import com.epam.BankService.Repository.CardsRepository;
import com.epam.BankService.mapper.MapperCards;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class CardsRepositoryImpl implements CardsRepository {

    @Autowired
    private DataSource dataSource;


    @Override
    public List<Cards> getAllCards() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("Select * From cards", new MapperCards());
    }
    @Override
    public List<Cards> getAllCardsById(int id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("Select * From cards where accountname_idaccountname= ?" ,new Object[]{id},  new MapperCards());
    }

    public Cards getAllCardsByAccId(int id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("Select * From cards where accountname_idaccountname= ?" ,new Object[]{id},  new MapperCards());
    }

    @Override
    public Cards getCardsById(int id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("Select * From cards Where idcards = ?", new Object[]{id}, new MapperCards());
    }

    @Override
    public void addCards(Cards cards, int idaccountname) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("numbercard", cards.getNumbercard())
                .addValue("balance", cards.getBalance())
                .addValue("accountname_idaccountname", idaccountname);
        namedParameterJdbcTemplate.update("Insert into banksys.cards (numbercard,balance,accountname_idaccountname) Values (:numbercard,:balance,:accountname_idaccountname)", sqlParameterSource);
    }

    @Override
    public void updateCard(Cards cards, int accountname_idaccountname) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Update cards Set numbercard = ?, balance = ? , accountname_idaccountname = ? Where idcards = ?",

                new Object[]{cards.getNumbercard(), cards.getBalance(),accountname_idaccountname,cards.getId()});

    }
    @Override
    public void removeCardsById(long id) {

    }

    @Override
    public void removeCardsByLogin(String login) {

    }

    @Transactional
    public void updateCards(Cards cards, int accountname_idaccountname) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Update cards Set numbercard = ?, balance = ? , accountname_idaccountname = ? Where idacards = ?",

                new Object[]{cards.getNumbercard(), cards.getBalance(),accountname_idaccountname,cards.getId()});

    }
}
