package com.epam.BankService.controller;

import com.epam.BankService.DTO.Accountname;
import com.epam.BankService.DTO.Cards;
import com.epam.BankService.DTO.User;
import com.epam.BankService.Manager.SessionUserManager;
import com.epam.BankService.Service.impl.AccountnameServiceImpl;
import com.epam.BankService.Service.impl.CardsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@Controller
public class HomeController {

    @Autowired
    private SessionUserManager sessionUserManager;

    @Autowired
    private AccountnameServiceImpl accountnameService;
    @Autowired
    private CardsServiceImpl cardsService;


    @GetMapping({"/","/home"})
    private ModelAndView home (ModelAndView modelAndView) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("home");
        return modelAndView;
    }

    @GetMapping("/balance")
    private ModelAndView balance (ModelAndView modelAndView) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);
        //вывод карт баланс
        List<Accountname> accountnames= accountnameService.getAllAccountnameById(user.getId());
        List<Cards> cards = new ArrayList<>();
        for(Accountname accountname : accountnames){
            if (accountname.isAccountstatus()){
                cards.add(cardsService.getAllCardsByAccId(accountname.getId()));
            }
        }
        modelAndView.addObject("cards", cards);
        modelAndView.setViewName("balance");
        return modelAndView;
    }
    @GetMapping("/story")
    private ModelAndView story (ModelAndView modelAndView) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);

        List<Accountname> accountnames= accountnameService.getAllAccountnameById(user.getId());
        List<Cards> cards = new ArrayList<>();
        for(Accountname accountname : accountnames){
            if (accountname.isAccountstatus()){
                cards.add(cardsService.getAllCardsByAccId(accountname.getId()));
            }
        }
        modelAndView.addObject("cards", cards);
        modelAndView.setViewName("story");
        return modelAndView;
    }
    @PostMapping("/story")
    public ModelAndView story(ModelAndView modelAndView, Cards cards, int balance , HttpServletRequest request) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);
        int id = Integer.parseInt(request.getParameterValues("select")[0]);
        cards = cardsService.getCardsById(id);
        cards.setBalance(cards.getBalance()+ balance);
        cardsService.updateCard(cards, accountnameService.getAllAccountnameById(user.getId()).get(0).getId());


        modelAndView.setViewName("redirect:/story");
        return modelAndView;
    }
    @GetMapping("/oplata")
    private ModelAndView oplata (ModelAndView modelAndView) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);

        List<Accountname> accountnames= accountnameService.getAllAccountnameById(user.getId());
        List<Cards> cards = new ArrayList<>();
        for(Accountname accountname : accountnames){
            if (accountname.isAccountstatus()){
                cards.add(cardsService.getAllCardsByAccId(accountname.getId()));
            }
        }
        modelAndView.addObject("cards", cards);
        modelAndView.setViewName("oplata");
        return modelAndView;
    }
    @GetMapping("/profile")
    private ModelAndView profile (ModelAndView modelAndView) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("profile");
        return modelAndView;
    }
    @GetMapping("/cards")
    private ModelAndView cards (ModelAndView modelAndView) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);

        List<Accountname> accountnames= accountnameService.getAllAccountnameById(user.getId());
        modelAndView.addObject("accountnames", accountnames);
        modelAndView.setViewName("cards");
        return modelAndView;
    }

    @PostMapping("profile")
    public ModelAndView profile(ModelAndView modelAndView, Accountname accountname ) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);

        accountname.setAccountstatus(true);
        accountnameService.addAccountname(accountname , user.getId());
        modelAndView.setViewName("profile");
        return modelAndView;
    }
    @PostMapping("cards")
    public ModelAndView cards(ModelAndView modelAndView, Cards cards, HttpServletRequest request) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);
        int id = Integer.parseInt(request.getParameterValues("select")[0]);
        cardsService.addCards(cards, id);
        modelAndView.setViewName("redirect:/cards");
        return modelAndView;
    }
    @GetMapping("/admin")
    private ModelAndView admin(ModelAndView modelAndView) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);

        List<Accountname> accountnames= accountnameService.getAllAccountname();
        modelAndView.addObject("nameaccount", accountnames);

        modelAndView.setViewName("admin");
        return modelAndView;
    }

    @PostMapping("/admin")
    public ModelAndView admin(ModelAndView modelAndView, Accountname accountname, HttpServletRequest request  ) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);
        int id = Integer.parseInt(request.getParameterValues("select")[0]);
        accountname = accountnameService.getAccountnameById(id);

        if(accountname.isAccountstatus()){
            accountname.setAccountstatus(false);
        }
        else accountname.setAccountstatus(true);
        accountnameService.updateAccountname(accountname, user.getId());
        modelAndView.setViewName("redirect:/admin");
        return modelAndView;
    }

   /* @PostMapping("oplata")
    public ModelAndView oplata(ModelAndView modelAndView, Cards cards, int summ, HttpServletRequest request) {
        User user = sessionUserManager.getCurrentSessionUser();
        modelAndView.addObject("user", user);
        cards = cardsService.
        int id = Integer.parseInt(request.getParameterValues("select")[0]);
        Cards cards1 = cardsService.getCardsById(id);
        cards1.setBalance(summ);
        cards1
        modelAndView.setViewName("redirect:/oplata");
        return modelAndView;
    }
*/
}
