package com.epam.BankService.controller;

import com.epam.BankService.DTO.Role;
import com.epam.BankService.DTO.User;
import com.epam.BankService.Manager.SessionUserManager;
import com.epam.BankService.Service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
    public class AuthenticationController {


        @Autowired
        UserServiceImpl userService;

        @Autowired
        SessionUserManager sessionUserManager;

        @GetMapping("login")
        public ModelAndView login(ModelAndView modelAndView) {
            modelAndView.setViewName("login");
            return modelAndView;
        }

        @PostMapping("login")
        public ModelAndView login(User user, ModelAndView modelAndView) {
            User foundUser = userService.authenticateUser(user);

            if (foundUser == null) {
                modelAndView.setViewName("welcome");
                return modelAndView;
            }
            sessionUserManager.setCurrentSessionUser(foundUser);
            return new ModelAndView("redirect:","user", foundUser);
        }

        @GetMapping("registration")
        public ModelAndView registration(ModelAndView modelAndView) {
            modelAndView.setViewName("registration");
            return modelAndView;
        }

        @PostMapping("registration")
        public ModelAndView registration(ModelAndView modelAndView, @Validated User user) {
            modelAndView.setViewName("registration");
            user.setRole(Role.CLIENT);
            userService.addUser(user);
            sessionUserManager.setCurrentSessionUser(user);
            return new ModelAndView("redirect:", "user", user);
        }

        @GetMapping("logout")
        public ModelAndView logout(ModelAndView modelAndView, HttpServletRequest httpServletRequest) {
            httpServletRequest.getSession().invalidate();
            modelAndView.setViewName("redirect:/login");
            return modelAndView;
        }
    }