package com.epam.BankService.interceptor;

import com.epam.BankService.DTO.User;
import com.epam.BankService.Manager.SessionUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

public class UserAwareInterceptor extends HandlerInterceptorAdapter {



    @Autowired

    private SessionUserManager sessionUserManager;



    @Override

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)

            throws Exception {

        User user = sessionUserManager.getCurrentSessionUser();



        if (Objects.nonNull(user)) {

            request.setAttribute("user", user);

        }



        return true;



    }



}