package com.epam.BankService.Service;

import com.epam.BankService.DTO.User;
import com.epam.BankService.exception.UserAlreadyExistException;

public interface UserService {

    void addUser(User user) throws UserAlreadyExistException;

    User authenticateUser(User user);

    Boolean checkLoginExist(String login);
}
