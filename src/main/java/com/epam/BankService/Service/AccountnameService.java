package com.epam.BankService.Service;

import com.epam.BankService.DTO.Accountname;
import com.epam.BankService.exception.AccountnameAlreadyExistException;

import java.util.List;

public interface AccountnameService {

    void addAccountname( Accountname accountname, int users_idusers) ;

    public List<Accountname> getAllAccountname( );

    Accountname authenticateAccountname(Accountname accountname );

    Boolean checkLoginExist(String login);

    public void updateAccountname(Accountname accountname, int users_idusers);

    public Accountname getAccountnameById(int id);
}
