package com.epam.BankService.Service;

import com.epam.BankService.DTO.Cards;
import com.epam.BankService.exception.CardsAlreadyExistException;

import java.util.List;

public interface CardsService {

    void addCards(Cards cards, int idaccountname);

    public Cards getAllCardsByAccId(int id);

    public List<Cards> getAllCardsById(int id);

    Cards authenticateCards(Cards cards );

    public Cards getCardsById(int id);

    Boolean checkLoginExist(String login);

    public void updateCards(Cards cards, int accountname_idaccountname);

    public void updateCard(Cards cards, int accountname_idaccountname);
}
