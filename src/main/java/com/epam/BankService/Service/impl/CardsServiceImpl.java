package com.epam.BankService.Service.impl;

import com.epam.BankService.DTO.Cards;
import com.epam.BankService.Repository.CardsRepository;
import com.epam.BankService.Repository.impl.CardsRepositoryImpl;
import com.epam.BankService.Service.CardsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CardsServiceImpl implements CardsService {
    @Autowired
    private CardsRepositoryImpl cardsRepository;
    @Override
    public void addCards(Cards cards , int idaccountname) {
        cardsRepository.addCards(cards, idaccountname);
    }
    @Override
    public Cards getAllCardsByAccId(int id) {
        return cardsRepository.getAllCardsByAccId(id);
    }
    @Override
    public List<Cards> getAllCardsById(int id) {
        return cardsRepository.getAllCardsById(id);
    }
    @Override
    public Cards getCardsById(int id) {
        return cardsRepository.getCardsById(id);
    }

    @Override
    public Cards authenticateCards(Cards cards) {
        return null;
    }

    @Override
    public Boolean checkLoginExist(String login) {
        return null;
    }
    @Override
    public void updateCard(Cards cards, int accountname_idaccountname){
        cardsRepository.updateCard(cards,accountname_idaccountname);
    }

    @Transactional
    public void updateCards(Cards cards, int accountname_idaccountname){}

}
