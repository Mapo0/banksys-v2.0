package com.epam.BankService.Service.impl;

import com.epam.BankService.DTO.User;
import com.epam.BankService.Repository.UserRepository;
import com.epam.BankService.Repository.impl.UserRepositoryImpl;
import com.epam.BankService.Service.UserService;
import com.epam.BankService.exception.UserAlreadyExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements  UserService{

    private final UserRepositoryImpl usersRepository;

    @Autowired
    public UserServiceImpl(UserRepositoryImpl usersRepository) {
        this.usersRepository = usersRepository;
    }
    @Override
    public void addUser(User user){
        usersRepository.addUser(user);
    }
    @Override
    public User authenticateUser(User user) {
        User foundUser = usersRepository.getUserByLogin(user.getLogin());

        if (!foundUser.getPassword().equals(user.getPassword())) {
            return null;
        }

        return foundUser;
    }


    @Override
    public Boolean checkLoginExist(String login) {
        return null;
    }
}