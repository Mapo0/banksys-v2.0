package com.epam.BankService.Service.impl;

import com.epam.BankService.DTO.Accountname;
import com.epam.BankService.Repository.impl.AccountnameRepositoryImpl;
import com.epam.BankService.Service.AccountnameService;
import com.epam.BankService.exception.AccountnameAlreadyExistException;
import com.epam.BankService.mapper.MapperAccountname;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountnameServiceImpl implements AccountnameService {

    @Autowired
    private AccountnameRepositoryImpl accountnameRepository;

    @Override
    public void addAccountname(Accountname accountname, int users_idusers) {
        accountnameRepository.addAccountname(accountname, users_idusers);
    }

    public List<Accountname> getAllAccountnameById(int id) {
        return accountnameRepository.getAllAccountnameById(id);
    }

    @Override
    public Accountname authenticateAccountname(Accountname accountname) {
        return null;
    }

    @Override
    public Boolean checkLoginExist(String login) {
        return null;
    }

    @Override
    public List<Accountname> getAllAccountname() {
        return accountnameRepository.getAllAccountname();
    }

    @Override
    public void updateAccountname(Accountname accountname, int users_idusers) {
        accountnameRepository.updateAccountname(accountname, users_idusers);

    }

    @Override
    public Accountname getAccountnameById(int id) {
        return accountnameRepository.getAccountnameById(id);

    }

}
