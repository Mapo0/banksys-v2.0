<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Bank</title>
        <link rel="shortcut icon" href="images/logo.png" type="image/png">
        <link rel="stylesheet" type="text/css" href="css/style.css">

    </head>
    <body>
    <div class="main">
    <form action="login" method="post" id="loginForm">
        <h1>Добро пожаловать</h1>
        <p>Login:</p>
        <input id="login" type="text" name="login" placeholder="login">
        <p>Password:</p>
        <input id="password" type="password" name="password" placeholder="Pssword">
        <a  href="home"><button id="log_in">Вход</button>
        </form>
        <a  href="registration"><button id="reg">Регистрация</button></a>

    </div>
    	<br>
    	<br>
    ${error}
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/script.js"></script>
</html>