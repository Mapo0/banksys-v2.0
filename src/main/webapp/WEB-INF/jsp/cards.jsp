<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<%@include file="home.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bank</title>
    <link rel="shortcut icon" href="images/logo.png" type="image/png">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<form action = "cards" method ="post">
<h>Создать карту:</h>
 <p>Введите номер карты</p>
<input id="card" type="text" name="numbercard" placeholder="карта" maxlength="8"  >
<p>Введите баланс:</p>
<input id="balance" type="text" name="balance" placeholder="баланс" maxlength="6" >Rub
<p>Выберите счёт:</p>
<select name = "select">
		<c:forEach var="accountnames" items="${accountnames}">
          <option value="${accountnames.id}">${accountnames.numberaccount}</option>
		</c:forEach>
</select>
<p><input type="submit" value="Завести карту"></p>
</form>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/script.js"></script>
</html>