<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<%@include file="home.jsp"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bank</title>
    <link rel="shortcut icon" href="images/logo.png" type="image/png">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

    <c:forEach var="cards" items="${cards}">
    <table>
        <tr>
        <th>Номер карты</th>
            <th>Сумма</th>
            </tr>
            <tr>
            <c:if test = "${salary > 2000}">
                     <p>My salary is:  <c:out value = "${salary}"/><p>
             </c:if>
            <td>${cards.numbercard}</td>
            <td>${cards.balance}RUB</td>
            </tr>
        </tr>
    </table>
    </c:forEach>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/script.js"></script>
</html>