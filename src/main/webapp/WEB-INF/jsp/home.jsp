<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Bank</title>
        <link rel="shortcut icon" href="images/logo.png" type="image/png">
        <link rel="stylesheet" type="text/css" href="css/style.css">
 </head>
<body>

       <div class="column side"></div>
       <div class="column middle">
           <div class="header">
               <h1>Банк</h1>
           </div>

      <ul id="bar">
             <li><a href="home">Главная</a></li>
             <li><a href="balance">Баланс</a></li>
             <li><a href="story">Пополнить счёт</a></li>
             <li><a href="oplata">Переводы</a></li>

             <c:if test="${user.username==null}">
                 <li style="float:right"><a href="login">Выйти</a></li>
             </c:if>
             <c:if test="${user.role == 'ADMIN'}">
                  <li style="float:right"><div><a href="/admin">Кнопка для админа</a></div>
                     </c:if>
             <c:if test="${user.username!=null}">
                 <li style="float:right"><a href="">${user.username}</a>
                     <ul>
                         <a href="profile">Мой профиль</a>
                         <a href="cards">Карта</a>
                         <a href="logout">Выйти</a>
                     </ul>
                 </li>
             </c:if>
         </ul>


</body>
</html>